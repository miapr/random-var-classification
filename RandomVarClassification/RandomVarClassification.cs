﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RandomVarClassification
{
    public class RandomVarClassification
    {
        private const int _randomNumbersCount = 10000;
        private readonly int _numbersCount;
        private readonly int[,] _classBorders = {{-100, 500}, {200, 750}};

        public RandomVarClassification(int numbersCount)
        {
            _numbersCount = numbersCount;
        }

        public List<List<double>> Classify(double firstProbability, double secondProbability)
        {
            return new List<List<double>>
            {
                Classify(firstProbability,0).ToList(),
                Classify(secondProbability,1).ToList()
            };
        }

        private IEnumerable<double> Classify(double probability, int borderIndex)
        {
            var result = new double[_numbersCount];
            var numbers = GenerateRandomNumbers(_classBorders[borderIndex, 0], _classBorders[borderIndex, 1]);
            var expectedValue = GetExpectedValue(numbers);
            var dispersion = GetDispersion(numbers, expectedValue);

            for (var i = 0; i < _numbersCount; i++)
                 result[i] = GetResult(probability, expectedValue, dispersion, i);

            return result;
        }

        public double[] GetErrors(double[] firstResults, double[] secondResult, bool isFirstProbability)
        {
            var minIndex = GetMinDifferenceIndex(firstResults, secondResult);
            return isFirstProbability
                ? new[] {secondResult.Take(minIndex).Sum(), firstResults.Skip(minIndex).Sum()}
                : new[] {firstResults.Take(minIndex).Sum(), secondResult.Take(minIndex).Sum()};
        }

        private IEnumerable<int> GenerateRandomNumbers(int low, int high)
        {
            var randomNumbers = new int[_randomNumbersCount];
            var random = new Random();

            for (var i = 0; i < _randomNumbersCount; i++)
            {
                randomNumbers[i] = random.Next(low, high);
            }

            return randomNumbers;
        }

        private double GetExpectedValue(IEnumerable<int> randomNumbers) //математическое ожидание
        {
            var sum = randomNumbers.Sum();
            return sum / _randomNumbersCount;
        }

        private double GetDispersion(IEnumerable<int> randomNumbers, double expectedValue)
        {
            var sum = randomNumbers.Sum(number => Math.Pow(number - expectedValue, 2));
            return Math.Sqrt(sum / _randomNumbersCount);
        }

        private double GetResult(double probability, double expectedValue, double dispersion, double x)
        {
            return probability * Math.Exp(-Math.Pow((x - expectedValue) / dispersion, 2) / 2) / (dispersion * Math.Sqrt(Math.PI * 2));
        }

        private int GetMinDifferenceIndex(double[] firstResults, double[] secondResult)
        {
            var minDifference = double.MaxValue;
            var minIndex = 0;
            for (var i = 0; i < _numbersCount; i++)
            {
                var difference = Math.Abs(firstResults[i] - secondResult[i]);
                if (minDifference > difference)
                {
                    minIndex = i;
                    minDifference = difference;
                }
            }

            return minIndex;
        }
    }
}