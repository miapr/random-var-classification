﻿namespace RandomVarClassification
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.graphBox = new System.Windows.Forms.PictureBox();
            this.countButton = new System.Windows.Forms.Button();
            this.firstProbabilityTextBox = new System.Windows.Forms.TextBox();
            this.secondProbabilityTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pc1Label = new System.Windows.Forms.Label();
            this.pc2Label = new System.Windows.Forms.Label();
            this.falseAlarmLabel = new System.Windows.Forms.Label();
            this.missDetectionLabel = new System.Windows.Forms.Label();
            this.sumErrorLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.graphBox)).BeginInit();
            this.SuspendLayout();
            // 
            // graphBox
            // 
            this.graphBox.BackColor = System.Drawing.SystemColors.Control;
            this.graphBox.Location = new System.Drawing.Point(12, 112);
            this.graphBox.Name = "graphBox";
            this.graphBox.Size = new System.Drawing.Size(854, 375);
            this.graphBox.TabIndex = 0;
            this.graphBox.TabStop = false;
            // 
            // countButton
            // 
            this.countButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.countButton.Location = new System.Drawing.Point(185, 49);
            this.countButton.Name = "countButton";
            this.countButton.Size = new System.Drawing.Size(183, 23);
            this.countButton.TabIndex = 1;
            this.countButton.Text = "Построить графики";
            this.countButton.UseVisualStyleBackColor = true;
            this.countButton.Click += new System.EventHandler(this.countButton_Click);
            // 
            // firstProbabilityTextBox
            // 
            this.firstProbabilityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.firstProbabilityTextBox.Location = new System.Drawing.Point(64, 33);
            this.firstProbabilityTextBox.Name = "firstProbabilityTextBox";
            this.firstProbabilityTextBox.Size = new System.Drawing.Size(100, 22);
            this.firstProbabilityTextBox.TabIndex = 2;
            this.firstProbabilityTextBox.TextChanged += new System.EventHandler(this.firstProbability_TextChanged);
            // 
            // secondProbabilityTextBox
            // 
            this.secondProbabilityTextBox.Enabled = false;
            this.secondProbabilityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.secondProbabilityTextBox.Location = new System.Drawing.Point(64, 66);
            this.secondProbabilityTextBox.Name = "secondProbabilityTextBox";
            this.secondProbabilityTextBox.Size = new System.Drawing.Size(100, 22);
            this.secondProbabilityTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Введите вероятности:";
            // 
            // pc1Label
            // 
            this.pc1Label.AutoSize = true;
            this.pc1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pc1Label.Location = new System.Drawing.Point(17, 36);
            this.pc1Label.Name = "pc1Label";
            this.pc1Label.Size = new System.Drawing.Size(41, 16);
            this.pc1Label.TabIndex = 5;
            this.pc1Label.Text = "P(C1)";
            // 
            // pc2Label
            // 
            this.pc2Label.AutoSize = true;
            this.pc2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pc2Label.Location = new System.Drawing.Point(17, 69);
            this.pc2Label.Name = "pc2Label";
            this.pc2Label.Size = new System.Drawing.Size(41, 16);
            this.pc2Label.TabIndex = 6;
            this.pc2Label.Text = "P(C2)";
            // 
            // falseAlarmLabel
            // 
            this.falseAlarmLabel.AutoSize = true;
            this.falseAlarmLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.falseAlarmLabel.Location = new System.Drawing.Point(385, 33);
            this.falseAlarmLabel.Name = "falseAlarmLabel";
            this.falseAlarmLabel.Size = new System.Drawing.Size(206, 16);
            this.falseAlarmLabel.TabIndex = 7;
            this.falseAlarmLabel.Text = "Вероятность ложной тревоги: ";
            // 
            // missDetectionLabel
            // 
            this.missDetectionLabel.AutoSize = true;
            this.missDetectionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.missDetectionLabel.Location = new System.Drawing.Point(385, 52);
            this.missDetectionLabel.Name = "missDetectionLabel";
            this.missDetectionLabel.Size = new System.Drawing.Size(251, 16);
            this.missDetectionLabel.TabIndex = 8;
            this.missDetectionLabel.Text = "Вероятность пропуска обнаружения:";
            // 
            // sumErrorLabel
            // 
            this.sumErrorLabel.AutoSize = true;
            this.sumErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sumErrorLabel.Location = new System.Drawing.Point(385, 72);
            this.sumErrorLabel.Name = "sumErrorLabel";
            this.sumErrorLabel.Size = new System.Drawing.Size(245, 16);
            this.sumErrorLabel.TabIndex = 9;
            this.sumErrorLabel.Text = "Суммарная ошибка классификации: ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 518);
            this.Controls.Add(this.sumErrorLabel);
            this.Controls.Add(this.missDetectionLabel);
            this.Controls.Add(this.falseAlarmLabel);
            this.Controls.Add(this.pc2Label);
            this.Controls.Add(this.pc1Label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.secondProbabilityTextBox);
            this.Controls.Add(this.firstProbabilityTextBox);
            this.Controls.Add(this.countButton);
            this.Controls.Add(this.graphBox);
            this.Name = "MainForm";
            this.Text = "Random Variable Classification";
            ((System.ComponentModel.ISupportInitialize)(this.graphBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox graphBox;
        private System.Windows.Forms.Button countButton;
        private System.Windows.Forms.TextBox firstProbabilityTextBox;
        private System.Windows.Forms.TextBox secondProbabilityTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label pc1Label;
        private System.Windows.Forms.Label pc2Label;
        private System.Windows.Forms.Label falseAlarmLabel;
        private System.Windows.Forms.Label missDetectionLabel;
        private System.Windows.Forms.Label sumErrorLabel;
    }
}

