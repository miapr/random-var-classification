﻿using System.Collections.Generic;
using System.Drawing;

namespace RandomVarClassification
{
    public class AlgorithmVisualization
    {
        private readonly int _width;

        private readonly int _height;

        private const int scale = 300;

        private const int borders = 10;

        private readonly RandomVarClassification _randomVarClassification;

        public AlgorithmVisualization(int width, int height)
        {
            _width = width;
            _height = height;
            _randomVarClassification = new RandomVarClassification(_width);
        }

        public double[] Visualize(double firstProbability, double secondProbability, Graphics graphics)
        {
            var points = _randomVarClassification.Classify(firstProbability, secondProbability);
            DrawLines(graphics);
            DrawGraph(points[0].ToArray(),graphics,Color.BlueViolet);
            DrawGraph(points[1].ToArray(), graphics, Color.Red);

            return _randomVarClassification.GetErrors(points[0].ToArray(), points[1].ToArray(),
                firstProbability > secondProbability);
        }

        private void DrawLines(Graphics graphics)
        {
            var pen = new Pen(Color.Black);
            graphics.DrawLine(pen, 0, _height - borders, _width, _height - borders);
            graphics.DrawLine(pen, _width, _height - borders, _width - borders, _height - 2*borders);
            graphics.DrawLine(pen, _width, _height - borders, _width - borders, _height);
            graphics.DrawLine(pen, borders, _height, borders, 0);
            graphics.DrawLine(pen, borders, 0, 2*borders, borders);
            graphics.DrawLine(pen, borders, 0, 0, borders);
        }

        private void DrawGraph(IReadOnlyList<double> points, Graphics graphics, Color color)
        {
            var pen = new Pen(color);

            for (var i = 1; i < _width; i++)
            {
                graphics.DrawLine(pen, i - 1, _height - (float) points[i - 1] * _height * scale - borders, i, _height - (float) points[i] * _height * scale - borders);
            }
        }
    }
}