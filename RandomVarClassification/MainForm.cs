﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace RandomVarClassification
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void countButton_Click(object sender, EventArgs e)
        {
            ChangeText();
            double firstProbability;
            double secondProbability;
            if (double.TryParse(firstProbabilityTextBox.Text, out firstProbability) &&
                double.TryParse(secondProbabilityTextBox.Text, out secondProbability))
            {
                
                var algoritmVisualization = new AlgorithmVisualization(graphBox.Width, graphBox.Height);
                using (var grapics = graphBox.CreateGraphics())
                {
                    grapics.Clear(Color.FromKnownColor(KnownColor.Control));
                    AddErrors(algoritmVisualization.Visualize(firstProbability, secondProbability, grapics));
                }
            }
            else
            {
                MessageBox.Show("Проверьте введенные данные.");
            }
        }

        private void firstProbability_TextChanged(object sender, EventArgs e)
        {
            ChangeText();
        }

        private void ChangeText()
        {
            double firstProbability;
            var result = double.TryParse(firstProbabilityTextBox.Text, out firstProbability) && firstProbability >= 0 &&
                         firstProbability <= 1
                ? secondProbabilityTextBox.Text = (1 - firstProbability).ToString()
                : secondProbabilityTextBox.Text = "";
        }

        private void AddErrors(double[] errors)
        {
            falseAlarmLabel.Text = $"Вероятность ложной тревоги: {errors[0].ToString("F3")}";
            missDetectionLabel.Text = $"Вероятность пропуска обнаружения: {errors[1].ToString("F3")}";
            sumErrorLabel.Text = $"Суммарная ошибка классификации {(errors[0] + errors[1]).ToString("F3")}";
        }
    }
}
